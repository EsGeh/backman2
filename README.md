# backman2 - Memorise shell commands

backman2 is a command line tool to help keeping track of how to call cmd line programs for repeating TASKs.
For every such TASK, backman2 keeps a config file with the name of a command and its arguments.
When running a TASK, this happens with certain standardised defaults for logging and where to store files.

## Motivation

An example use case are regular backups using rsync.
Would you want to remember all options and paths every time you want to run your backup?
Hmm. And where did you store your logs last time...?

## Features

- nice, convenient command line interface
- useful defaults how to run TASKs:

	- stdout written into logfiles in a standardised directory and filename
	- every command is run in a standardised working directory, where you may keep config files etc.

## Usage

	$ backman --help

The command line interface is pretty self-explanatory. Subcommands accept the `--help` option as well.

## Examples / Tutorial

- List TASKs:

		$ backman ls
	
	Nothing there yet? Maybe you want to...

- Add a TASK:

		$ backman add tick fish -c "while true; sleep 1; echo tick; end" --

	This creates a new TASK named "tick" as a shortcut for the command `fish -c "while true; sleep 1; echo tick; end"`

- Show a TASK:

		$ backman show tick
	
	This tool makes a distinction between OPTIONS and PARAMETERS. This task has only OPTIONS, no PARAMETERS.

- Run a TASKs:

		$ backman run tick

	Nothing happens, but the Terminal hangs. By default, the output is redirected into a log file.
	Interrupt the program by typing ^C (Ctrl+C).

- Show log files:

	Where is our log file? Let's see:

		$ backman log ls tick
		> .../.config/backman2/tick/log/tick_2020-01-01_16:16:16.log
	
	Let's inspect the content of the log file:

		$ backman log cat tick
	
	Aha, there it is!

- Working directories:

	Lets add a TASK that writes something to a file:

		$ backman add -f tick_to_file fish -c "rm -f tick.out; while true; sleep 1; echo tick | tee --apend tick.out; end" --
	
	Run it and abort it after a few seconds:

		$ backman run tick_to_file
		^C
	
	This script was supposed to create a file named `tick.out`. But where is it?

		$ backman wd tick_to_file
		> .../.config/backman2/tick_to_file/wd
	
	To lazy `cd`ing there and inspecting the file? There is an easier way:

		$ backman wd tick_to_file ls
		> tick.out
	
	Aha! There it is. Instead of `ls` you may append any other command. The command is then executed with the working directory set first, e.g. this will open a subshell in the commands dedicated working directory:

		$ backman wd tick_to_file fish # or any other shell (bash, zsh, ...)
	
	You may look around, inspect files and make changes. When you are done, leave the subshell by typing ^C.

## Supported Operation Systems

- Linux

Might work on other OSes if all dependent tools are provided. So far only tested on Linux.

## Dependencies

- [Python](https://www.python.org/)

## Install and test locally

	$ python -m venv venv
	$ ./venv/bin/activate
	(venv)$ pip install -e .[dev]

run tests:

	$ pytest

check test coverage:

	$ pytest --cov=src tests

## Create Python Distribution Package

	$ python -m venv venv
	$ ./venv/bin/activate
	(venv)$ pip install build
	(venv)$ python -m build

## Create Package for the arch Linux Distribution

	$ mkdir arch && cp PKGBUILD arch/ && cd arch/
	(arch/)$ makepkg -s

To install the package:

	(arch/)$ makepkg -i

## Reference

The following sources were used as inspiration or reference:

- [setuptools](https://setuptools.pypa.io/): Create distributable python packages
- [pytest](https://pytest.org/): Elegant test framework for python
- [Arch - Python package guidelines](https://wiki.archlinux.org/title/Python_package_guidelines)
- [click](https://click.palletsprojects.com/): a much better alternative than [argparse](https://docs.python.org/3/library/argparse.html). Argparse is simply not fulfill the requirements of advanced command line tools like this.

# Remarks:

[click](https://click.palletsprojects.com/) should be preferred over [argparse](https://docs.python.org/3/library/argparse.html).
Why?

Because argparse is:

- Badly designed
- Tries to be clever about how to distinguish options and parameters, which makes it unusable for complex situations in "wrapper" tools which pass options through to other tools
- Doesn't stick to Unix standards and shoots itself in the foot (see previous point)
