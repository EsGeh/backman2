from pathlib import Path
import time as time_module
from importlib import metadata
import shlex


PACKAGE_NAME = "backman2"
PACKAGE_VERSION = metadata.version( PACKAGE_NAME )

CONFIG_DIR_DEF=Path.home() / ".config" / "backman2"

CONFIG_DIR = CONFIG_DIR_DEF

# output templates:
SEP_STR="------------------------"
START_TIME_MSG = "time: {time}"

HEADING_LOG = (
    "***************************************\n"
    "* COMMAND: {command_name}\n"
    "* PACKAGE: {package_name} v. {package_version}\n"
    "***************************************\n"
    "time: {time}"
)
RUN_CND_LOG = "run:\n  {command}"
START_CMD_OUTPUT_LOG = "---\nOUTPUT START:"
END_CMD_OUTPUT_LOG = (
        "OUTPUT END\n"
        "---"
)
INT_SIG_LOG = "INTERRUPTED. Caught signal {intsig}"
EXIT_CODE_LOG = "exit code: {exit_code}"
END_TIME_LOG = "time: {time}"

ADD_FILE_MSG = "add file '{path}'"
RM_FILE_MSG = "rm '{path}'"
CAT_FILE_MSG = "cat '{path}'"
EDITED_FILE_MSG = "'{path}' changed"
RUN_CND_MSG = "run:\n  {command} >{log_path}"
MK_DIR_MSG = "mkdir '{path}'"

# Errors:
FILE_NOT_EXISTING_ERR = "'{path}' does not exist"
FILE_EXISTING_ERR = "'{path}' already exists"
ERROR_RUNNING_COMMAND_ERR = "'{cmd_name}' running command failed with error: {error}"
USER_NOT_FOUND_ERR = "user not found: '{user}'"
CMD_NOT_FOUND_ERR = "command not found: '{command}'"
CMD_FAILED_ERR = "command failed. exit code: {exit_code}"
LOG_NOT_FOUND_ERR = "no log found in {log_dir}"

ERROR_MSG = "ERROR: {error}"

def command_in_messages(
        argv
):
    return " ".join(map(
        shlex.quote,
        argv,
    ))

def cmd_dir(
        config_dir,
        cmd_name,
):
    return config_dir / cmd_name

def wd_dir(
        config_dir,
        cmd_name,
):
    return config_dir / cmd_name / "wd"

def log_dir(
        config_dir,
        cmd_name,
):
    return cmd_dir( config_dir, cmd_name ) / "log"

def log_path(
        config_dir,
        cmd_name,
        now,
):
    time_str = time_module.strftime(
            "%Y-%m-%d_%H:%M:%S",
            now
    )
    return log_dir( config_dir, cmd_name ) / f"{cmd_name}_{time_str}.log"
