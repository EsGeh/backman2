from backman import _constants as constants

import sys


class StopScriptException( Exception ):
    pass

def init(
        config_dir,
        verbose,
):
    if verbose:
        if config_dir.is_dir():
            print(
                    constants.FILE_EXISTING_ERR.format(
                        path=config_dir
                    ),
                    file=sys.stderr,
            )
        else:
            print(
                    constants.MK_DIR_MSG.format(
                        path=config_dir
                    ),
                    file=sys.stderr,
            )
    config_dir.mkdir(
            exist_ok=True,
            parents=True,
    )

def check_was_initialised(
        config_dir,
        verbose,
):
    if not config_dir.is_dir():
        raise StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_dir
                )
        )
