from backman.commands import subcommands
from backman import _constants as constants
from backman import _utils as utils

import time
from importlib import metadata
import yaml
import click
from functools import update_wrapper
import sys

COMMAND_NAME = next(filter(
        lambda entry: entry.value.startswith( __name__ ),
        metadata.entry_points( group="console_scripts" ),
)).name

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


class Group( click.Group ):
    def format_commands(self, ctx: click.Context, formatter: click.HelpFormatter):
        commands = []
        for subcommand in self.list_commands(ctx):
            cmd = self.get_command(ctx, subcommand)
            # What is this, the tool lied about a command.  Ignore it
            if cmd is None:
                continue
            if cmd.hidden:
                continue

            commands.append((subcommand, cmd))

        # allow for 3 times the default spacing
        if len(commands):
            limit = formatter.width - 6 - max(len(cmd[0]) for cmd in commands)

            rows = []
            for subcommand, cmd in commands:
                help = cmd.get_short_help_str(limit)
                rows.append((subcommand, help))

            if rows:
                with formatter.section("ACTIONs"):
                    formatter.write_dl(rows)

def main():
    cli(
            obj={}
    )

############################
# Root group
############################

@click.group(
        cls = Group,
        context_settings=CONTEXT_SETTINGS,
        subcommand_metavar="ACTION [ARGS ...]",
)
@click.option(
        "-c", "--config", "config_dir",
        type=click.Path(),
        default=constants.CONFIG_DIR_DEF,
        help="the directory, where to store all memorised commands",
        show_default=True,
)
@click.option(
        "-v", "--verbose", "verbose",
        default=False,
        is_flag=True,
        show_default=True,
)
@click.pass_context
def cli(
        ctx,
        config_dir,
        verbose,
):
    """Memorise TASKs in config files. A TASK is a description how to run some shell command. TASKS are run in a standardised environment including defaults for logging and where to store config files"""
    ctx.ensure_object( dict )
    ctx.obj = dict(
            config_dir=config_dir,
            verbose=verbose,
    )

############################
# init command
############################

@cli.command()
@click.pass_obj
def init(
        args
):
    """list all memorised tasks"""
    subcommands.init( **args )

############################
# ls command
############################

@cli.command()
@click.pass_obj
def ls(
        args
):
    """list all memorised tasks"""
    run_command( subcommands.ls, args )

############################
# show command
############################

@cli.command()
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.pass_obj
def show(
        args,
        cmd_name,
):
    """show memorised task TASK"""
    args['cmd_name'] = cmd_name
    run_command( subcommands.show, args )

############################
# add command
############################

@cli.command(
        context_settings={
            "ignore_unknown_options": True,
            "allow_interspersed_args": False,
        },
)
@click.option(
        "-f", "--force", "overwrite",
        default=False,
        is_flag=True,
        show_default=True,
)
@click.option(
        "--user", "user",
)
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.argument(
        "cmd",
)
@click.argument(
        "rest_args",
        metavar="[PARAMS ...] | [OPTS ... -- PARAMS ...]]",
        nargs=-1,
        type=click.UNPROCESSED,
)
@click.pass_obj
def add(
        args,
        overwrite,
        user,
        cmd,
        cmd_name,
        rest_args,
):
    """memorise TASK as a shortcut for calling
    
        $ CMD OPTS ... PARAMS ...

    REMARK:
    When running the add command, seperate OPTS from PARAMS via '--', if possible.
    (OPTS are arguments that start with '-').
    When '--' is not found, all arguments are considered PARAMS.
    """
    cmd_opts, cmd_params, args_has_separator = parse_rest_args(
            rest_args,
    )
    if not args_has_separator:
        cmd_params=cmd_opts[::]
        cmd_opts=[]
    args.update(dict(
            cmd_name=cmd_name,
            overwrite=overwrite,
            cmd=cmd,
            cmd_opts=cmd_opts,
            cmd_params=cmd_params,
            config_dir=args['config_dir'],
            user=user,
    ))
    run_command( subcommands.add, args )

############################
# rm command
############################

@cli.command()
@click.option(
        "--rm-conf", "rm_config",
        default=False,
        is_flag=True,
        show_default=True,
)
@click.option(
        "--all", "rm_all",
        default=False,
        is_flag=True,
        show_default=True,
)
@click.option(
        "--rm-wd", "rm_wd_dir",
        default=False,
        is_flag=True,
        show_default=True,
)
@click.option(
        "--rm-logs", "rm_log_dir",
        default=False,
        is_flag=True,
        show_default=True,
)
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.pass_obj
def rm(
        args,
        rm_config,
        rm_wd_dir,
        rm_log_dir,
        rm_all,
        cmd_name,
):
    """delete TASK"""
    args['cmd_name'] = cmd_name
    if rm_all:
        rm_config = True
        rm_wd_dir = True
        rm_log_dir = True
    args['rm_config'] = rm_config
    args['rm_wd_dir'] = rm_wd_dir
    args['rm_log_dir'] = rm_log_dir
    run_command( subcommands.rm, args )

############################
# edit command
############################

@cli.command()
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.pass_obj
def edit(
        args,
        cmd_name,
):
    """edit TASK with your favourite editor"""
    args['cmd_name'] = cmd_name
    run_command( subcommands.edit, args )

############################
# run command
############################

@cli.command(
        context_settings={
            "ignore_unknown_options": True,
            "allow_interspersed_args": False,
        },
)
@click.option(
        "-n", "--dry-run", "dry_run",
        is_flag=True,
        default=False,
)
@click.option(
        "--tee", "tee",
        default=False,
        is_flag=True,
)
@click.option(
        "--no-log", "no_log",
        default=False,
        is_flag=True,
)
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.argument(
        "rest_args",
        metavar="[APPEND_OPTS ... [-- APPEND_PARAMS ...]]",
        nargs=-1,
        type=click.UNPROCESSED,
)
@click.pass_obj
def run(
        args,
        cmd_name,
        rest_args,
        dry_run,
        tee,
        no_log,
):
    """Run TASK.
    The memorised command has the following form:

        $ CMD OPTS ... PARAMS ...

    APPEND_OPTS or APPEND_PARAMS will be appended AFTER the memorised OPTS / PARAMS:

        $ CMD OPTS ... APPEND_OPTS ... PARAMS ... APPEND_PARAMS ...
    """
    cmd_opts, cmd_params, _args_has_separator = parse_rest_args(
            rest_args,
    )
    now = time.gmtime()
    args.update(dict(
            cmd_name=cmd_name,
            append_opts=cmd_opts,
            append_params=cmd_params,
            now=now,
            dry_run=dry_run,
            tee=tee,
            no_log=no_log,
    ))
    run_command( subcommands.run, args, exit_code_on_fail=2 )

############################
# wd command
############################

@cli.command(
        context_settings={
            "ignore_unknown_options": True,
            "allow_interspersed_args": False,
        },
)
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.argument(
        "cmd",
        default="pwd",
)
@click.argument(
        "rest_args",
        metavar="[ARGS ...]",
        nargs=-1,
        type=click.UNPROCESSED,
)
@click.pass_obj
def wd(
        args,
        cmd_name,
        cmd,
        rest_args,
):
    """run some command inside TASKs working dir"""
    args.update(dict(
        cmd_name=cmd_name,
        cmd=cmd,
        cmd_args=rest_args,
    ))
    run_command( subcommands.wd_run, args )

############################
# log group
############################

@cli.group(
        subcommand_metavar="ACTION [ARGS ...]",
        cls = Group,
)
def log(
):
    pass

############################
# log ls command
############################

@log.command(name="ls")
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.pass_obj
def log_ls(
        args,
        cmd_name,
):
    args.update(dict(
        cmd_name=cmd_name,
    ))
    run_command( subcommands.log_ls, args )

############################
# log cat command
############################

@log.command(name="cat")
@click.argument(
        "cmd_name",
        metavar="TASK",
)
@click.pass_obj
def log_cat(
        args,
        cmd_name,
):
    """output last log"""
    args.update(dict(
        cmd_name=cmd_name,
    ))
    run_command( subcommands.log_cat, args )

############################
# utils
############################

def run_command(
        command,
        args,
        exit_code_on_fail=1
):
    try:
        utils.check_was_initialised(
            args['config_dir'],
            args['verbose'],
        )
        ret = command(
                **args
        )
        sys.exit( ret )
    except utils.StopScriptException as error:
        print(
                constants.ERROR_MSG.format(
                    error=error
                )
        )
        sys.exit(exit_code_on_fail)

def parse_rest_args(
        rest_args,
):
    rest_args = [*rest_args]
    append_opts = []
    append_params = []
    parse_opts=True
    for arg in rest_args:
        if arg == "--":
            parse_opts=False
        elif parse_opts:
            append_opts.append( arg )
        else:
            append_params.append( arg )
    return append_opts, append_params, not parse_opts
