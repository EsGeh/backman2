from backman import _constants as constants
from backman import _utils as utils


def run(
        config_dir,
        **_kwargs,
):
    configs = filter(
            lambda f: f.name.endswith( ".conf" ),
            config_dir.iterdir(),
    )
    for f in configs:
        print( f.stem )
