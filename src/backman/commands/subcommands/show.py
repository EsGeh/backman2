from backman import _constants as constants
from backman import _utils as utils


def run(
        cmd_name,
        config_dir,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    print(config_path.read_text(), end="")
