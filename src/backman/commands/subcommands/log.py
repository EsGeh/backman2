from backman import _constants as constants
from backman import _utils as utils

import sys


def log_ls(
        cmd_name,
        config_dir,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    log_dir = constants.log_dir( config_dir, cmd_name )
    for f in sorted(log_dir.iterdir()):
        print( f )

def log_cat(
        cmd_name,
        config_dir,
        verbose,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    log_dir = constants.log_dir( config_dir, cmd_name )
    sorted_logs = sorted(log_dir.iterdir(), reverse=True)
    if len(sorted_logs) == 0:
        raise utils.StopScriptException(
                constants.LOG_NOT_FOUND_ERR.format(
                    log_dir=log_dir
                )
        )
    last_log = sorted_logs[0]
    if verbose:
        print(
                constants.CAT_FILE_MSG.format(
                    path=last_log,
                ),
                file=sys.stderr,
        )
    print(last_log.read_text(), end="")
