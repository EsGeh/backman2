from backman import _constants as constants
from backman import _utils as utils

import click
import sys
import subprocess


def wd_run(
        cmd_name,
        cmd,
        cmd_args,
        config_dir,
        verbose,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    wd_dir = constants.wd_dir(
            config_dir,
            cmd_name,
    )
    proc_ret = 0
    argv = [cmd]
    argv.extend( cmd_args )
    try:
        proc_ret = subprocess.run(
                argv,
                check=False,
                cwd=wd_dir,
        ).returncode
    # This usually means: Command not found (?)
    except FileNotFoundError:
        print(
                constants.CMD_NOT_FOUND_ERR.format(
                    command=cmd,
                ),
                file=sys.stderr,
        )
        return 127
    # Command could not be executed by OS for other reasons...(?)
    except OSError as error:
        print(
                constants.ERROR_RUNNING_COMMAND_ERR.format(
                    cmd_name=cmd_name,
                    error=error,
                ),
                file=sys.stderr,
        )
        return 126
    except subprocess.SubprocessError as error:
        print(
                constants.ERROR_RUNNING_COMMAND_ERR.format(
                    cmd_name=cmd_name,
                    error=error,
                ),
                file=sys.stderr,
        )
        return 126
    if proc_ret!= 0:
        print(
                constants.CMD_FAILED_ERR.format(
                    exit_code=proc_ret,
                ),
                file=sys.stderr,
        )
    return proc_ret
