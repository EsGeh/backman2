from backman import _constants as constants
from backman import _utils as utils

import yaml
import shlex
import subprocess
import pwd
import sys
import io
import time
import signal


# exit codes under Linux:
# 126: Command invoked cannot execute
# 127: Comand not found
def run(
        cmd_name,
        config_dir,
        now,
        dry_run,
        tee,
        no_log,
        **kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    with open(config_path, "r") as file:
        config = yaml.safe_load(file)
    cmd=[config['command']]
    options = config.get('options', [])
    options.extend( kwargs.get('append_opts', []) )
    parameters = config.get('parameters', [])
    parameters.extend( kwargs.get('append_params', []) )
    cmd.extend([
            *options,
            *parameters,
    ])
    user=config.get('user')
    if user is not None:
        if user.isdigit():
            user = int( user )
        try:
            if isinstance(user, int):
                pwd.getpwuid( user )
            else:
                pwd.getpwnam( user )
        except KeyError:
            print(
                    constants.USER_NOT_FOUND_ERR.format(
                        user=user,
                    )
            )
            return 126
    log_path = constants.log_path(
            config_dir,
            cmd_name,
            now
    )
    cmd_str = constants.command_in_messages(
        cmd,
    )
    if kwargs['verbose']:
        print(
                constants.RUN_CND_MSG.format(
                    command=cmd_str,
                    log_path=shlex.quote(str(log_path)),
                ),
                file=sys.stderr,
        )
    # exit_code = 0
    proc_info = dict(
            exit_code = 0,
            interrupted = None,
    )
    if dry_run:
        return 0
    log_file = None
    try:
        if not no_log:
            log_file = open(log_path, "w")
            ## log heading:
            print(
                    constants.HEADING_LOG.format(
                        command_name = "backman",
                        package_name = constants.PACKAGE_NAME,
                        package_version = constants.PACKAGE_VERSION,
                        time = time.strftime(
                            "%Y-%m-%d_%H:%M:%S",
                            now,
                        ),
                    ),
                    file = log_file,
            )
            print(
                    constants.RUN_CND_LOG.format(
                        command=cmd_str,
                    ),
                    file = log_file,
            )
            print(
                    constants.START_CMD_OUTPUT_LOG,
                    file = log_file,
            )
        wd_dir = constants.wd_dir(
                config_dir,
                cmd_name,
        )
        proc = subprocess.Popen(
                cmd,
                user=user,
                # check=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                cwd=wd_dir,
        )
        def interrupt_handler( signum, frame ):
            proc_info['interrupted'] = signum
            # redirect the signal to the subprocess:
            proc.send_signal( signum )
        signal.signal(signal.SIGINT, interrupt_handler)
        while proc.poll() is None:
            line = proc.stdout.readline()
            if line:
                if tee:
                    sys.stdout.buffer.write( line )
                    sys.stdout.flush()
                if log_file is not None:
                    log_file.write( line.decode("utf-8") )
        proc_info['exit_code'] =  proc.returncode
        write_log_appendix(
                log_file=log_file,
                verbose=kwargs['verbose'],
                **proc_info,
        )
    # This usually means: Command not found (?)
    except FileNotFoundError:
        err_msg = constants.ERROR_MSG.format(error=(constants.CMD_NOT_FOUND_ERR.format(
            command=config['command'],
        )))
        print( err_msg, file=log_file )
        print( err_msg, file=sys.stderr )
        return 127
    # Command could not be executed by OS for other reasons...(?)
    except (OSError, subprocess.SubprocessError) as error:
        err_msg = constants.ERROR_MSG.format(error=(constants.ERROR_RUNNING_COMMAND_ERR.format(
            cmd_name=cmd_name,
            error=error,
        )))
        print( err_msg, file=log_file )
        print( err_msg, file=sys.stderr )
        return 126
    finally:
        if log_file is not None:
            log_file.close()
    return proc_info['exit_code']

def write_log_appendix(
        log_file,
        verbose,
        exit_code,
        interrupted = None,
):
    end_time = time.strftime(
        "%Y-%m-%d_%H:%M:%S",
        time.gmtime(),
    )
    print(
            constants.END_CMD_OUTPUT_LOG,
            file = log_file,
    )
    if interrupted is not None:
        msg = constants.INT_SIG_LOG.format(
                intsig=interrupted,
        )
        print( msg, file=log_file )
        if verbose:
            print( msg, file=sys.stderr )
        return
    msg = constants.EXIT_CODE_LOG.format(
                exit_code=exit_code
            )
    print( msg, file=log_file )
    if verbose:
        if exit_code != 0:
            print(
                    constants.ERROR_MSG.format(error=constants.CMD_FAILED_ERR.format(
                        exit_code=exit_code,
                    )),
                    file=sys.stderr,
            )
    print(
            constants.END_TIME_LOG.format(
                time=end_time
            ),
            file = log_file,
    )
