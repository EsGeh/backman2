from backman import _constants as constants
from backman import _utils as utils

import yaml
import sys


def run(
        cmd_name,
        overwrite,
        cmd,
        cmd_opts,
        cmd_params,
        config_dir,
        user,
        verbose,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if config_path.exists():
        if not overwrite:
            raise utils.StopScriptException(
                    constants.FILE_EXISTING_ERR.format(
                        path=config_path
                    )
            )
        config_path.unlink()
    config = dict(
            command=cmd,
            options=cmd_opts,
            parameters=cmd_params,
    )
    if user is not None:
        config['user'] = user
    if verbose:
        print(
                constants.ADD_FILE_MSG.format(
                    path=config_path,
                ),
                file=sys.stderr,
        )
    with open(config_path, "w") as file:
        config = yaml.dump(
                config,
                file,
                sort_keys=False,
        )
    # cmd dir
    cmd_dir = constants.cmd_dir(
            config_dir,
            cmd_name,
    )
    if not cmd_dir.exists():
        if verbose:
            print(
                    constants.ADD_FILE_MSG.format(
                        path=cmd_dir,
                    ),
                    file=sys.stderr,
            )
        cmd_dir.mkdir(
                parents=True,
        )
    # wd dir
    wd_dir = constants.wd_dir(
            config_dir,
            cmd_name,
    )
    if not wd_dir.exists():
        if verbose:
            print(
                    constants.ADD_FILE_MSG.format(
                        path=wd_dir,
                    ),
                    file=sys.stderr,
            )
        wd_dir.mkdir(
                parents=True,
        )
    # log dir
    log_dir = constants.log_dir(
            config_dir,
            cmd_name,
    )
    if not log_dir.exists():
        if verbose:
            print(
                    constants.ADD_FILE_MSG.format(
                        path=log_dir,
                    ),
                    file=sys.stderr,
            )
        log_dir.mkdir(
                parents=True,
        )
