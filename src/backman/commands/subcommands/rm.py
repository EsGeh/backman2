from backman import _constants as constants
from backman import _utils as utils

import sys
import shutil

def run(
        cmd_name,
        config_dir,
        rm_config,
        rm_wd_dir,
        rm_log_dir,
        verbose,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    if rm_config:
        if verbose:
            print(
                    constants.RM_FILE_MSG.format(
                        path=config_path,
                    ),
                    file=sys.stderr,
            )
        config_path.unlink()
    # wd dir:
    if rm_wd_dir:
        wd_dir = constants.wd_dir(
                config_dir,
                cmd_name,
        )
        if wd_dir.is_dir():
            if verbose:
                print(
                        constants.RM_FILE_MSG.format(
                            path=wd_dir,
                        ),
                        file=sys.stderr,
                )
            shutil.rmtree( wd_dir )
    # log dir:
    if rm_log_dir:
        log_dir = constants.log_dir(
                config_dir,
                cmd_name,
        )
        if log_dir.is_dir():
            if verbose:
                print(
                        constants.RM_FILE_MSG.format(
                            path=log_dir,
                        ),
                        file=sys.stderr,
                )
            shutil.rmtree( log_dir )
    # cmd dir:
    cmd_dir = constants.cmd_dir(
            config_dir,
            cmd_name,
    )
    # if cmd_dir is empty now, delete it too:
    if cmd_dir.is_dir() and not list(cmd_dir.iterdir()):
        if verbose:
            print(
                    constants.RM_FILE_MSG.format(
                        path=cmd_dir,
                    ),
                    file=sys.stderr,
            )
        cmd_dir.rmdir()
