from backman import _constants as constants
from backman import _utils as utils


def run(
        config_dir,
        verbose,
        **_kwargs,
):
    utils.init(
            config_dir,
            verbose,
    )
