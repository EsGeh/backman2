from backman import _constants as constants
from backman import _utils as utils

import click
import sys


def run(
        cmd_name,
        config_dir,
        verbose,
        **_kwargs,
):
    config_path = config_dir / f"{cmd_name}.conf"
    if not (config_path.exists() and config_path.is_file()):
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_ERR.format(
                    path=config_path
                )
        )
    try:
        click.edit(
                filename=str(config_path)
        )
    except click.UsageError as error:
        raise utils.StopScriptException from error
    if verbose:
        print(
                constants.EDITED_FILE_MSG.format(
                    path=config_path,
                ),
                file=sys.stderr,
        )
