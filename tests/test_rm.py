from backman import _utils as utils
from backman.commands.subcommands.rm import run as rm
from backman import _constants as constants
from tests import utils as test_utils

import pytest


command = rm

@pytest.fixture
def default_params(
        tmp_path,
):
    return dict(
            cmd_name = "test",
            config_dir = tmp_path / "config",
            verbose = False,
            rm_config = False,
            rm_wd_dir = False,
            rm_log_dir = False,
    )

@pytest.fixture
def config_dict():
    return dict(
            command="test_cmd",
            options=[],
            parameters=[
                "arg1",
                "arg2",
                "arg3",
            ],
    )

def test_run(
        config_dict,
        default_params,
        capsys,
):
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    command(
            **params
    )
    config_path = params['config_dir'] / f"{params['cmd_name']}.conf"
    assert config_path.exists()
    # wd dir
    assert constants.wd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # log dir
    assert constants.log_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # cmd dir
    assert constants.cmd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_rm_config(
        config_dict,
        default_params,
        capsys,
):
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['rm_config'] = True
    command(
            **params
    )
    config_path = params['config_dir'] / f"{params['cmd_name']}.conf"
    assert not config_path.exists()
    # wd dir
    assert constants.wd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # log dir
    assert constants.log_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # cmd dir
    assert constants.cmd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_rm_wd(
        config_dict,
        default_params,
        capsys,
):
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['rm_wd_dir'] = True
    command(
            **params
    )
    config_path = params['config_dir'] / f"{params['cmd_name']}.conf"
    assert config_path.exists()
    # wd dir
    assert not constants.wd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # log dir
    assert constants.log_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # cmd dir
    assert constants.cmd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_rm_log(
        config_dict,
        default_params,
        capsys,
):
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['rm_log_dir'] = True
    command(
            **params
    )
    config_path = params['config_dir'] / f"{params['cmd_name']}.conf"
    assert config_path.exists()
    # wd dir
    assert constants.wd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # log dir
    assert not constants.log_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # cmd dir
    assert constants.cmd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_rm_wd_and_log(
        config_dict,
        default_params,
        capsys,
):
    """deleting wd and log dirs should also delete the whole cmd dirs"""
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['rm_wd_dir'] = True
    params['rm_log_dir'] = True
    command(
            **params
    )
    config_path = params['config_dir'] / f"{params['cmd_name']}.conf"
    assert config_path.exists()
    # wd dir
    assert not constants.wd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # log dir
    assert not constants.log_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    # cmd dir
    assert not constants.cmd_dir(
            params['config_dir'],
            params['cmd_name'],
    ).is_dir()
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_fails_if_cmd_not_exists(
        config_dict,
        default_params,
        capsys,
):
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['cmd_name'] = "non_existent_cmd"
    with pytest.raises( utils.StopScriptException ):
        command(
                **params
        )
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_run_verbose(
        config_dict,
        default_params,
        capsys,
):
    params = default_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['verbose'] = True
    params['rm_config'] = True
    command(
            **params
    )
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err != ""
