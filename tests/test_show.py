from backman import _utils as utils
from backman.commands.subcommands.show import run as show
from tests import utils as test_utils

import pytest


command = show

@pytest.fixture
def show_params(
        tmp_path,
):
    return dict(
            cmd_name = "test1",
            config_dir = tmp_path / "config",
            verbose = False,
    )

@pytest.fixture
def config_dict():
    return dict(
            command="test_cmd",
            parameters=[
                "arg1",
                "arg2",
                "arg3",
            ],
    )

def test_run(
        config_dict,
        show_params,
        capsys,
):
    params = show_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    command(
            **params
    )
    captured=capsys.readouterr()
    assert captured.out.rstrip( "\n" ) == test_utils.config_dict_to_str(
            config_dict,
    )
    assert captured.err == ""

def test_fails_if_cmd_not_exists(
        tmp_path,
        config_dict,
        show_params,
        capsys,
):
    params = show_params
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    params['cmd_name'] = "non_existent"
    with pytest.raises( utils.StopScriptException ):
        command(
                **params
        )
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""
