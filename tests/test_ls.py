from backman import _utils as utils
from backman.commands.subcommands.ls import run as ls
from tests import utils as test_utils

import pytest


command = ls

@pytest.fixture
def ls_params(
        tmp_path,
):
    return dict(
            config_dir = tmp_path / "config",
            verbose = False,
    )

def test_ls(
        tmp_path,
        ls_params,
        capsys,
):
    params = ls_params
    # config_dir( params['config_dir'] )
    config_dict = dict(
            command="test_cmd",
            parameters=[
                "arg1",
                "arg2",
                "arg3",
            ],
    )
    test_utils.init_config(
            params['config_dir'],
            {
                "test1": config_dict,
                "test2": config_dict,
                "test3": config_dict,
            },
    )
    command(
            **ls_params
    )
    captured=capsys.readouterr()
    assert set(captured.out.splitlines()) == set([
            "test1",
            "test2",
            "test3",
    ])
