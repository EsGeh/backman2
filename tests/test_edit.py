from backman import _utils as utils
from backman.commands.subcommands.edit import run as edit
from tests import utils as test_utils

import pytest


command = edit

@pytest.fixture
def default_params(
        tmp_path,
):
    return dict(
            config_dir = tmp_path / "config",
            verbose = False,
    )

@pytest.fixture
def config_dict():
    return dict(
            command="test_cmd",
            options=[],
            parameters=[
                "arg1",
                "arg2",
                "arg3",
            ],
    )

def run_test(
        config_dict,
        default_params,
        capsys,
        monkeypatch,
        params,
):
    import click
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config_dict,
            },
    )
    mock_state = dict(
            editor_called=False
    )
    def mock_open_editor(
            text=None,
            editor=None,
            env=None,
            require_save=True,
            extension='.txt',
            filename=None,
    ):
        mock_state['editor_called'] = True
        mock_state['kwargs'] = dict(
                text=text,
                editor=editor,
                env=env,
                require_save=require_save,
                extension=extension,
                filename=filename,
        )
    monkeypatch.setattr( click, "edit", mock_open_editor )
    command(
            **params
    )
    return mock_state

def test_run(
        config_dict,
        default_params,
        capsys,
        monkeypatch,
):
    params = default_params
    params['cmd_name'] = "test"
    mock_state = run_test(
            config_dict,
            default_params,
            capsys,
            monkeypatch,
            params,
    )
    assert mock_state['editor_called'] == True
    assert mock_state['kwargs']['filename'] == str(params['config_dir'] / f"{params['cmd_name']}.conf")
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_verbose(
        config_dict,
        default_params,
        capsys,
        monkeypatch,
):
    import click
    params = default_params
    params['cmd_name'] = "test"
    params['verbose'] = True
    mock_state = run_test(
            config_dict,
            default_params,
            capsys,
            monkeypatch,
            params,
    )
    assert mock_state['editor_called'] == True
    assert mock_state['kwargs']['filename'] == str(params['config_dir'] / f"{params['cmd_name']}.conf")
    captured=capsys.readouterr()
    assert captured.out == ""
    assert captured.err != ""
