from backman import _utils as utils
from backman import _constants as constants
from backman.commands.subcommands.run import run
from tests import utils as test_utils

import pytest
import time


command = run

@pytest.fixture
def now():
    return time.gmtime()

@pytest.fixture
def run_params(
        tmp_path,
        now,
):
    return dict(
            cmd_name = "test",
            config_dir = tmp_path / "config",
            verbose = False,
            now = now,
            dry_run = False,
            tee = False,
            no_log = False,
    )

def echo_config():
    return dict(
            command="echo",
            parameters=[
                "hello world!",
            ],
    )

def init_config():
    return dict(
            command="echo",
            parameters=[
                "hello world!",
            ],
            init=dict(
                command="test",
                parameters=[
                    "-e",
                    "file",
                ],
                failmsg="file not found!"
            )
    )

def log_file_heading(
        now = now,
):
    return constants.HEADING_LOG.format(
            command_name = "backman",
            package_name = constants.PACKAGE_NAME,
            package_version = constants.PACKAGE_VERSION,
            time = time.strftime(
                "%Y-%m-%d_%H:%M:%S",
                now,
            ),
    )

def log_file_content(
        config,
        now, end_time,
        exit_code,
        content,
        append_opts=[],
        append_params=[],
        exception_msg=None,
):
    expected = "".join([
            log_file_heading( now ), "\n",
            constants.RUN_CND_LOG.format(
                command = constants.command_in_messages(
                    [config['command']]
                    + config.get("options", [])
                    + append_opts
                    + config.get("parameters", [])
                    + append_params
                ),
            ), "\n",
            constants.START_CMD_OUTPUT_LOG, "\n",
            content,
    ])
    if not exception_msg:
        expected += "".join([
                constants.END_CMD_OUTPUT_LOG, "\n",
                constants.EXIT_CODE_LOG.format(
                    exit_code=exit_code,
                ), "\n",
                constants.END_TIME_LOG.format(
                    time=time.strftime( "%Y-%m-%d_%H:%M:%S", end_time ),
                ), "\n",
        ])
    else:
        expected += (exception_msg + "\n")
    return expected

def test_run_echo(
        tmp_path,
        run_params,
        capfd,
):
    params = run_params
    config = echo_config()
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
            )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=0,
            content="hello world!\n",
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_run_exit_with_code(
        run_params,
        capfd,
):
    params = run_params
    config = dict(
            command="bash",
            parameters=[
                "-c",
                "exit 10",
            ],
    )
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 10
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="",
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_command_not_installed_on_os(
        run_params,
        capfd,
):
    params = run_params
    not_installed_cmd="non_existent_command"
    config = dict(
            command=not_installed_cmd,
            parameters=[
                "hallo welt",
            ],
    )
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 127
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    exception_msg = constants.ERROR_MSG.format(error=(constants.CMD_NOT_FOUND_ERR.format(
        command=not_installed_cmd,
    )))
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="",
            exception_msg=exception_msg,
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err != ""

def test_non_existent_command(
        run_params,
        capfd,
):
    params = run_params
    config = echo_config()
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    params['cmd_name'] = "non_existent"
    with pytest.raises( utils.StopScriptException ):
        command(
                **params
        )
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert not log_path.exists()
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_options(
        run_params,
        capfd,
):
    params = run_params
    config = dict(
            command="echo",
            options=["-n"],
            parameters=[
                "hello world!",
                "hello universe!",
            ],
    )
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="hello world! hello universe!"
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_append_opts(
        run_params,
        capfd,
):
    params = run_params
    config = echo_config()
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    # run as is:
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="hello world!\n",
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""
    # run with appended option: no newline:
    params['append_opts'] = ['-n']
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="hello world!",
            append_opts = [ "-n" ]
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_append_opts_and_params(
        run_params,
        capfd,
):
    params = run_params
    config = echo_config()
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    # run as is:
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="hello world!\n"
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""
    log_path.unlink()
    log_path.touch()
    # run with appended option: no newline:
    params['append_opts'] = ['-n']
    params['append_params'] = [ "hello universe!" ]
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="hello world! hello universe!",
            append_opts = [ "-n" ],
            append_params = [ "hello universe!" ],
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_working_dir(
        tmp_path,
        run_params,
        capfd,
):
    params = run_params
    config = dict(
            command="pwd",
    )
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert log_path.exists()
    assert log_path.is_file()
    wd_dir = constants.wd_dir(
            params['config_dir'],
            params['cmd_name']
    )
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content=f"{wd_dir}\n"
    )
    captured=capfd.readouterr()
    assert captured.out == ""
    assert captured.err == ""

def test_dry_run(
        tmp_path,
        run_params,
        capfd,
):
    params = run_params
    config = dict(
            command="cmd",
            options=[
                "-a",
                "-b",
                "--long-opt",
            ],
            parameters=[
                "x",
                "y",
                "z",
            ]
    )
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    params["dry_run"] = True
    params["verbose"] = True
    ret = command(
            **params
    )
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
    )
    assert not log_path.exists()
    captured=capfd.readouterr()
    assert captured.out == ""
    assert "cmd -a -b --long-opt x y z" in str(captured.err)

def test_tee(
        tmp_path,
        run_params,
        capfd,
):
    params = run_params
    config = echo_config()
    test_utils.init_config(
            params['config_dir'],
            {
                params['cmd_name']: config,
            },
    )
    params["tee"] = True
    ret = command(
            **params
    )
    end_time = time.gmtime()
    assert ret == 0
    log_path = constants.log_path(
            params['config_dir'],
            params['cmd_name'],
            now = params['now'],
            )
    assert log_path.exists()
    assert log_path.is_file()
    assert log_path.read_text() == log_file_content(
            config=config,
            now=params['now'],
            end_time=end_time,
            exit_code=ret,
            content="hello world!\n"
    )
    captured=capfd.readouterr()
    assert captured.out == "hello world!\n"
    assert captured.err == ""
