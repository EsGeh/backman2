from backman import _constants as constants

import pytest

import filecmp
import re
import io


def init_config(
        config_dir,
        config_dicts,
):
    config_dir.mkdir()
    for cmd_name, config_dict in config_dicts.items():
        config_str = config_dict_to_str( config_dict )
        (config_dir / f"{cmd_name}.conf").write_text(
                config_str
        )
        constants.cmd_dir(
            config_dir,
            cmd_name,
        ).mkdir(
                parents=True,
                exist_ok=True,
        )
        constants.wd_dir(
            config_dir,
            cmd_name,
        ).mkdir(
                parents=True,
                exist_ok=True,
        )
        constants.log_dir(
            config_dir,
            cmd_name,
        ).mkdir(
                parents=True,
                exist_ok=True,
        )

def config_dict_to_str(
        config_dict
):
    command = config_dict.get("command")
    options = config_dict.get("options")
    parameters = config_dict.get("parameters")
    dict_str = f"command: {command}"
    if options is not None:
        dict_str += f"\noptions:"
        for arg in options:
            dict_str += f"\n- {arg}"
    if parameters is not None:
        dict_str += f"\nparameters:"
        for arg in parameters:
            dict_str += f"\n- {arg}"
    return dict_str

########################
# utils:
########################
